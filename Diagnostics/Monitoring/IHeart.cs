namespace Lambda.Diagnostics.Monitoring
{
    public interface IHeart
    {
        void Beat();
        void Dispose();
        void Reset();

    }
}