﻿using System;
using System.Runtime.CompilerServices;
using System.Timers;
using Lambda.Extensions;

namespace Lambda.Diagnostics.Monitoring
{
    public class Heart : IHeart
    {
        #region fields

        private readonly Timer _timer;
        private int _currentBeat;
        private int _totalBeat;
        private int _totalCheck;
        private float _avarageBeat;
        private int _lastCheckedTotalBeat;
        private int _noPulseCount;
        public double Interval { get; private set; }
        public int NumberOfFaildIntervalsBeforeHeartattack { get; private set; }
        public bool IsAsync { get; private set; }

        #endregion

        #region public methods

        /// <summary>
        /// Heart is a simple mechanism to keep the rate of a piece of code
        /// </summary>
        /// <param name="interval">interval of checking the health of the heart. this will raise OnReport event</param>
        /// <param name="numberOfFaildIntervalsBeforeHeartattack">indicates the number of acceptable failed checks before the state changes into heartattack. in such cases OnHeartattack will fire.</param>
        /// <param name="isAsync">false will run the events on the same thread and true would fire them in separate threads</param>
        public Heart(double interval, int numberOfFaildIntervalsBeforeHeartattack = 1, bool isAsync = true)
        {
            Interval = interval;
            NumberOfFaildIntervalsBeforeHeartattack = numberOfFaildIntervalsBeforeHeartattack;
            IsAsync = isAsync;
            _timer = new Timer(interval);
            _timer.Elapsed += Check;
        }

        /// <summary>
        /// this method resets the heart stat
        /// </summary>
        public void Reset()
        {
            _noPulseCount = 0;
            _avarageBeat = 0;
            _currentBeat = 0;
            _totalBeat = 0;
            _totalCheck = 0;
            _lastCheckedTotalBeat = 0;
        }

        /// <summary>
        /// Calling this method will keep the heart alive
        /// </summary>
        public void Beat()
        {
            if (!_timer.Enabled)
            {
                _timer.Start();
                RaiseOnAlive();
            }
            _currentBeat++;
            _totalBeat++;
        }

        /// <summary>
        /// call this method in order to dispose the heart
        /// </summary>
        public void Dispose()
        {
            _timer.Stop();
            _timer.Dispose();
            RaiseOnDied();
        }

        #endregion

        #region private methods
        private void Check(object state, ElapsedEventArgs elapsedEventArgs)
        {
            RaiseOnPulse();
            if (_totalBeat == 0)
                return;
            _totalCheck++;
            if (_lastCheckedTotalBeat == _totalBeat)
            {
                _noPulseCount++;
                if (_noPulseCount > NumberOfFaildIntervalsBeforeHeartattack)
                {
                    Reset();
                    RaiseOnHeartAttack();
                    return;
                }

            }
            else
            {
                _noPulseCount = 0;
            }

            _lastCheckedTotalBeat = _totalBeat;
            _avarageBeat = (float)_totalBeat / _totalCheck;
            RaiseOnReport(_currentBeat, _avarageBeat);
            _currentBeat = 0;


        }
        #endregion

        #region events

        #region Pulse event

        private EventHandler _onPulse;

        public event EventHandler OnPulse
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            add { _onPulse = (EventHandler)Delegate.Combine(_onPulse, value); }
            [MethodImpl(MethodImplOptions.Synchronized)]
            remove { _onPulse = (EventHandler)Delegate.Remove(_onPulse, value); }
        }

        protected void RaiseOnPulse()
        {
            var handler = _onPulse;
            if (handler != null)
                handler.Invoke(this, EventArgs.Empty, IsAsync);
        }

        #endregion

        #region HeartAttack event

        private EventHandler _onHeartAttack;

        public event EventHandler OnHeartAttack
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            add { _onHeartAttack = (EventHandler)Delegate.Combine(_onHeartAttack, value); }
            [MethodImpl(MethodImplOptions.Synchronized)]
            remove { _onHeartAttack = (EventHandler)Delegate.Remove(_onHeartAttack, value); }
        }

        protected void RaiseOnHeartAttack()
        {
            var handler = _onHeartAttack;
            if (handler != null)
                handler.Invoke(this, EventArgs.Empty, IsAsync);
        }

        #endregion

        #region Alive event

        private EventHandler _onAlive;

        public event EventHandler OnAlive
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            add { _onAlive = (EventHandler)Delegate.Combine(_onAlive, value); }
            [MethodImpl(MethodImplOptions.Synchronized)]
            remove { _onAlive = (EventHandler)Delegate.Remove(_onAlive, value); }
        }

        protected void RaiseOnAlive()
        {
            var handler = _onAlive;
            if (handler != null)
                handler.Invoke(this, EventArgs.Empty, IsAsync);
        }

        #endregion

        #region Died event

        private EventHandler _onDied;

        public event EventHandler OnDied
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            add { _onDied = (EventHandler)Delegate.Combine(_onDied, value); }
            [MethodImpl(MethodImplOptions.Synchronized)]
            remove { _onDied = (EventHandler)Delegate.Remove(_onDied, value); }
        }

        protected void RaiseOnDied()
        {
            var handler = _onDied;
            if (handler != null)
                handler.Invoke(this, EventArgs.Empty, IsAsync);
        }

        #endregion

        #region Report event
        public class ReportEventArgs : EventArgs
        {
            public int CurrentHeartbeat { get; set; }
            public float AverageHeartbeat { get; set; }

            public ReportEventArgs(int currentHeartbeat, float averageHeartbeat)
            {
                CurrentHeartbeat = currentHeartbeat;
                AverageHeartbeat = averageHeartbeat;
            }
        }

        public delegate void ReportEventHandler(object sender, ReportEventArgs e);

        private ReportEventHandler _onReport;

        public event ReportEventHandler OnReport
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            add { _onReport = (ReportEventHandler)Delegate.Combine(_onReport, value); }
            [MethodImpl(MethodImplOptions.Synchronized)]
            remove { _onReport = (ReportEventHandler)Delegate.Remove(_onReport, value); }
        }

        protected void RaiseOnReport(int currentHeartbeat, float averageHeartbeat)
        {
            var handler = _onReport;
            var e = new ReportEventArgs(currentHeartbeat, averageHeartbeat);
            if (handler == null) return;
            if (IsAsync)
                handler.BeginInvoke(this, e, null, null);
            else
                handler.Invoke(this, e);
        }

        #endregion

        #endregion

    }
}
