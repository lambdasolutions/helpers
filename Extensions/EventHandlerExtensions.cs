using System;

namespace Lambda.Extensions
{
    public static class EventHandlerExtensions
    {
        /// <summary>
        /// invokes an eventhandler Sync or Async
        /// </summary>
        public static void Invoke<TEventArgs>(this EventHandler<TEventArgs> handler, Object sender, TEventArgs eventArgs,
            bool isAsync = false, AsyncCallback callback = null, object @object = null) where TEventArgs : EventArgs //where TEventHandler:delegate 
        {
            if (!isAsync)
            {
                handler(sender, eventArgs);
            }
            else
            {
                handler.BeginInvoke(sender, eventArgs, callback, @object);
            }
        }

        /// <summary>
        /// invokes an eventhandler Sync or Async
        /// </summary>
        public static void Invoke<TEventArgs>(this EventHandler handler, Object sender, TEventArgs eventArgs,
            bool isAsync = false, AsyncCallback callback = null, object @object = null) where TEventArgs : EventArgs //where TEventHandler:delegate 
        {
            if (!isAsync)
            {
                handler(sender, eventArgs);
            }
            else
            {
                handler.BeginInvoke(sender, eventArgs, callback, @object);
            }
        }
    }
}