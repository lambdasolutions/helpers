﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lambda.Extensions
{
    public static class DictionaryExtention
    {
        public static void AddOrUpdate<TK, TV>(this IDictionary<TK, TV> dictionary, TK key, TV value)
        {
            if(dictionary==null)
                throw new NullReferenceException();
            if (!dictionary.ContainsKey(key))
                dictionary.Add(key, value);
            else
                dictionary[key] = value;
        }
    }
}
